## Aim: To Demodulate the modulated signal.

## Theory:
Demodulation is a key process in the reception of any amplitude modulated signals.
Demodulation is the process by which the original information bearing signal, i.e. the modulation is extracted from the incoming overall received signal.
The process of demodulation for signals using amplitude modulation can be achieved in a number of different techniques, each of which has its own advantage.
The demodulator is the circuit, or for a software defined radio, the software that is used to recover the information content from the overall incoming modulated signal.
Detection or demodulation
The terms detection and demodulation are often used when referring to the overall demodulation process. Essentially the terms describe the same process, and the same circuits. 
Terms like diode detector, synchronous detector and product detector are widely used. But the term demodulation tends to be used more widely when referring to the process of extracting the modulation from the signal.<br>
<div align="center"> <img src="demod.png"/><br></div>
The term detection is the older term dating back to the early days of radio.
The term demodulation is probably more accurate in that it refers to the process of demodulation, i.e. extracting the modulation from the signal.
That said both terms can be used equally well, although modern terminology tends to err towards the use of the words demodulation and demodulator.<br>

<b>AM demodulation techniques</b><br>
There are a number of techniques that can be used to demodulate AM signals. Different types are used in different applications to suit their performance and cost.<br>
<b>Diode rectifier envelope detector:</b><br>   This form of detector is the simplest form, only requiring a single diode and a couple of other low cost components. The performance is adequate for low cost AM broadcast radios, but it does not meet the standards of other forms of demodulation.<br> 
<b>Product detector:</b><br>   It is possible to demodulate amplitude modulated signals with a receiver that incorporates a product detector of mixer and a local beat frequency oscillator or carrier injection oscillator. In its basic form, the local oscillator is not synchronised to the incoming signal carrier.<br> 
<b>Synchronous detection:</b><br>   Synchronous detection provides the optimum performance. It uses a mixer or product detector with a local oscillator signal that is synchronised to the incoming signal carrier. This provides many advantages over the other methods of AM demodulation.  


## Procedure:
1.	Students are asked to select the proper components required to rig up the circuit.
2.	Students will click on the connect button to connect the circuit.
3.	Students will be asked to enter the message and carrier signal.
4.	Students will click on CRO visual to generate the graph.
5.	Students will compare the demodulated graph and the message signal graph.
7.	Students will click on Test your Skill tab to test the knowledge they have gained during the experiment.<br>
<b>*Note: If any problem occurs during the experiment the student can refresh the page by clicking on Reset tab.</b>


